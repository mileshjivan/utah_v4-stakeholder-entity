/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Contractor Documents - Main Scenario",
        createNewBrowserInstance = false
)

public class FR7_Capture_Contractor_Documents_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR7_Capture_Contractor_Documents_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToContractorSupplierManager()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToContractorSupplierManager(){    
        //Navigate to Contractor Or Supplier Manager Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractorSupplierManager_Tab())){
            error = "Failed to wait for 'Contractor Or Supplier Manager' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contractorSupplierManager_Tab())){
            error = "Failed to click on 'Contractor Or Supplier Manager' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Contractor Or Supplier Manager' tab.");
        
        //Navigate to Documents panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.documents_Panel())){
            error = "Failed to wait for 'Questionnaires' panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.documents_Panel())){
            error = "Failed to click on 'Questionnaires' panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Contractor Or Supplier Manager' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.documents_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.documents_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Process Flow
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.questionnaireProcessFlow())) {
//            error = "Failed to wait for 'Process Flow' button.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.questionnaireProcessFlow())) {
//            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.questionnaireProcessFlow())) {
//                error = "Failed to wait for 'Process Flow' button field.";
//                return false;
//            }
//            pause(5000);
//            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.questionnaireProcessFlow())) {
//                error = "Failed to click on 'Process Flow' button field.";
//                return false;
//            }
//        }
        
        //Document Type
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.documents_Input())){
            error = "Failed to wait for 'Documents Type' field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.documents_Input(), getData("Documents Type"))){
            error = "Failed to enter '" + getData("Documents Type") + "' into 'Documents Type' field.";
            return false; 
        }
        
        //Document Uploaded
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.documentsUploaded_Dropdown())){
            error = "Failed to wait for 'Documents Uploaded' dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.documentsUploaded_Dropdown())){
            error = "Failed to click on 'Documents Uploaded' dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.documentsUploaded_Dropdown_Option(getData("Documents Uploaded")))){
            error = "Failed to wait for '" + getData("Documents Uploaded") + "' option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.documentsUploaded_Dropdown_Option(getData("Documents Uploaded")))){
            error = "Failed to click on '" + getData("Documents Uploaded") + "' into 'Documents Uploaded' option.";
            return false; 
        }
        
        //Outcome
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.outcome_Dropdown())){
            error = "Failed to wait for 'Outcome' dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.outcome_Dropdown())){
            error = "Failed to click on 'Outcome' dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.outcome_Dropdown_Option(getData("Outcome")))){
            error = "Failed to wait for '" + getData("Outcome") + "' option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.outcome_Dropdown_Option(getData("Outcome")))){
            error = "Failed to click on '" + getData("Outcome") + "' option.";
            return false; 
        }
        
        //Date verified
if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.dateVerifiedTab())){
            error = "Failed to wait for 'Date verified' field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.dateVerifiedTab(), getData("Date Verified"))){
            error = "Failed to click on '" + getData("Date Verified") + "' field.";
            return false; 
        }
        
        
        //comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.commentsTab())){
            error = "Failed to wait for 'Comments' field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.commentsTab(), getData("Comments"))){
            error = "Failed to click on '" + getData("Comments") + "' field.";
            return false; 
        }
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.documents_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.documents_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        pause(10000);
        narrator.stepPassedWithScreenShot("Successfully saved contractor documents.");
        
        //Check if the record has been Saved
//        if(SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup())){
//            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup());
//
//            if(saved.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//            }else{   
//                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.failed())){
//                    error = "Failed to wait for error message.";
//                    return false;
//                }
//
//                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.failed());
//
//                if(failed.equals("ERROR: Record could not be saved")){
//                    error = "Failed to save record.";
//                    return false;
//                }
//            }
//        }
        

        
        return true;
    }
   
}
