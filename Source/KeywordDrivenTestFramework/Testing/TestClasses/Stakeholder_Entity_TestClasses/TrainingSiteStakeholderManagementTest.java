/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.TrainingSiteStakeholderManagementPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Click on Stakeholders",
    createNewBrowserInstance = false
)
public class TrainingSiteStakeholderManagementTest extends BaseClass
{

    String error = "";

    public TrainingSiteStakeholderManagementTest()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!navigateToAPageFromStakeholderManagementPage())
        {
            return narrator.testFailed("Failed to navigate to a module from Stakeholder Management page" + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from Stakeholder Management page");
    }

   
    public boolean navigateToAPageFromStakeholderManagementPage() throws InterruptedException
    {
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteStakeholderManagementPageObjects.iframeXpath()))     
//        {
//            error = "Failed to locate iframe";
//            return false;
//        }
//        
//               SeleniumDriverInstance.wait(10000);
//        
//        if (!SeleniumDriverInstance.switchToFrameByXpath(TrainingSiteStakeholderManagementPageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to iframe";
//            return false;
//        }

//        if (!SeleniumDriverInstance.hoverOverElementByXpath(TrainingSiteStakeholderManagementPageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to iframe";
//            return false;
//        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteStakeholderManagementPageObjects.linkForAPageInHomePageXpath(testData.getData("StakeholderManagementPageName")))) {
            error = "Failed to locate the module: "+testData.getData("StakeholderManagementPageName");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteStakeholderManagementPageObjects.linkForAPageInHomePageXpath(testData.getData("StakeholderManagementPageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("StakeholderManagementPageName");
            return false;
        }

        
        return true;

    }

}
