/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNAgel
 */

@KeywordAnnotation(
        Keyword = "Add_View Engagements - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Add_View_Engagements_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Add_View_Engagements_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToEngagements())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
//        if(!EngagementWindow()){
//            return narrator.testFailed("Failed due - " + error);
//        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToEngagements(){
        //Left Arrow
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to wait for 'Left' Arrow.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to click on 'Left' Arrow.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
        
       //Right arrow
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.sik_rightarrow())){
//              error = "Failed to click right arrow.";
//              return false;
//          }
//        
//        for (int i = 0; i < 3; i++) {
//         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.sik_rightarrow())){
//              error = "Failed to click right arrow.";
//              return false;
//          }
//        }
        
        //Navigate to Engaments Tab
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementsTab())){
            error = "Failed to wait for 'Engagements' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.engagementsTab())){
            error = "Failed to click on 'Engagements' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements' tab.");
        
        return true;
    }
    
       public boolean enterDetails(){
        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
         
        //Add an engagement button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.addEngagementsButton())){
            error = "Failed to wait for 'Add Engagements' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.addEngagementsButton())){
            error = "Failed to click on 'Add Engagements' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add Engagements' button.");
        
        //switch to the new window
        pause(10000);
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch on the 'Engagements' window.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched to the 'Engagements' window..");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.processFlow())){
            error = "Failed to wait for 'Engagement' Process Flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.processFlow())){
            error = "Failed to click on 'Engagement' Process Flow.";
            return false;
        }        
        narrator.stepPassedWithScreenShot("Successfully click 'Engagement' Process Flow.");        
        
        
         //Engagement Title
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementTitle())){
            error = "Failed to wait for 'Engagement Title' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.engagementTitle(), getData("Engagement Title"))){
            error = "Failed to enter '" + getData("Engagement Title") + "' into 'Engagement Title' input.";
            return false;
        }
        
        //Engagement Date
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_engagementDate())){
            error = "Failed to wait for 'Engagement Date' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.si_engagementDate(), testData.getData("Engagement Date"))){
            error = "Failed to click 'Engagement Date' input field.";
            return false;
        } 
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.businessUnitTab())){
            error = "Failed to wait for 'Business Unit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.businessUnitTab())){
            error = "Failed to click on 'Business Unit' tab.";
            return false;
        }
        
         //Select all 
         pause(1000);
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.businessUnit_SelectAll())){
                error = "Failed to wait for 'Business Unit' tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.businessUnit_SelectAll())){
                error = "Failed to click on 'Select All' Business Unit.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Select All' Business Unit."); 
        
         //Project Link checkbox
        if (getData("Project Link").equalsIgnoreCase("YES")) {
            //Check project link Checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.projectLink())) {
                error = "Failed to wait for 'Project link' check box.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.projectLink())) {
                error = "Failed to click on 'Project link' check box.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Project Link successfully checked.");

            //Project
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.projectTab())) {
                error = "Failed to wait for 'Project' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.projectTab())) {
                error = "Failed to click on 'Project' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to wait for '" + getData("Project") + "' list.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to click on '" + getData("Project") + "' list.";
                return false;
            }
            SeleniumDriverInstance.pause(2000);
            narrator.stepPassedWithScreenShot("Project : '" + getData("Project") + "'.");
        }
        
        //Business Unit
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.businessUnitTab())){
//            error = "Failed to wait for 'Business Unit' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.businessUnitTab())){
//            error = "Failed to click on 'Business Unit' tab.";
//            return false;
//        }
//        //Expand Global Company
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.expandGlobalCompany())){
//            error = "Failed to wait for 'Global Company' expansion";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.expandGlobalCompany())){
//            error = "Failed to wait for 'Global Company' expansion";
//            return false;
//        }
//        //Expand South Africa
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.expandSouthAfrica())){
//            error = "Failed to wait for 'South Africa' expansion";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.expandSouthAfrica())){
//            error = "Failed to wait for 'South Africa' expansion";
//            return false;
//        }
//        
//        if (getData("Select All").equalsIgnoreCase("YES")){
//            //Select all 
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.businessUnit_GlobalCompany())){
//                error = "Failed to wait for 'Business Unit' tab.";
//                return false;
//            }
//            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.businessUnit_SelectAll())){
//                error = "Failed to click on 'Select All' Business Unit.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully click 'Select All' Business Unit."); 
//        }else{
//            //Victory Site
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.businessUnitSelect(getData("Business Unit")))){
//                error = "Failed to wait for Business Unit option: '" + getData("Business Unit") + "'.";
//                return false;
//            }
//            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.businessUnitSelect(getData("Business Unit")))){
//                error = "Failed to select Business Unit option: '" + getData("Business Unit") + "'.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Business unit : '" + getData("Business Unit") + "'.");
//        }
//         
//        //Specific location
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.specificLocation())){
//            error = "Failed to wait for 'Specific location' input.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.specificLocation(), getData("Specific Location"))){
//            error = "Failed to enter '" + getData("Specific Location") + "' into 'Specific location' input.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Specific Location : '" + getData("Specific Location") + "' .");
//        
//        //Project Link checkbox
//        if (getData("Project Link").equalsIgnoreCase("YES")) {
//            //Check project link Checkbox
//            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.projectLink())) {
//                error = "Failed to wait for 'Project link' check box.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.projectLink())) {
//                error = "Failed to click on 'Project link' check box.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Project Link successfully checked.");
//
//            //Project
//            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.projectTab())) {
//                error = "Failed to wait for 'Project' tab.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.projectTab())) {
//                error = "Failed to click on 'Project' tab.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyProject(getData("Project")))) {
//                error = "Failed to wait for '" + getData("Project") + "' list.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyProject(getData("Project")))) {
//                error = "Failed to click on '" + getData("Project") + "' list.";
//                return false;
//            }
//            SeleniumDriverInstance.pause(2000);
//            narrator.stepPassedWithScreenShot("Project : '" + getData("Project") + "'.");
//        }
//        
//        //Show map checkbox
//         if (getData("Show map").equalsIgnoreCase("YES")) {
//             if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ShowMapLink())) {
//                error = "Failed to wait for 'Show map' check box.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ShowMapLink())) {
//                error = "Failed to click on 'Show map' check box.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Show map successfully checked.");
//         }
         
//        //Engagement date
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementDate())){
//            error = "Failed to wait for 'Engagement date' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.engagementDate(), getData("Engagement date"))){
//            error = "Failed to enter '" + getData("Engagement date")+ "' into 'Engagement date' field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Engagement date : '" + getData("Engagement date")+ "'.");
//         
//        //Communication with
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.communicationWithTab())){
//            error = "Failed to wait for 'Communication with' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.communicationUsers())){
//            error = "Failed to wait for 'Communication with' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.communicationWith_SelectAll())){
//            error = "Failed to click on 'Select All' Communication with.";
//            return false;
//        }
//        
//        //Users
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.usersTab())){
//            error = "Failed to wait for 'Users' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.usersTab())){
//            error = "Failed to click on 'Users' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.userOption(getData("Users")))){
//            error = "Failed to wait for '" + getData("Users") + "' user.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.userOption(getData("Users")))){
//            error = "Failed to click on '" + getData("Users") + "' user.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.usersTab())){
//            error = "Failed to click on 'Users' tab.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Users : '" + getData("Users")+ "'.");
//        
//        //stackeholders
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.stakeholdersTab())){
//            error = "Failed to wait for 'Stakeholders' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.stakeholdersTab())){
//            error = "Failed to click on 'Stakeholders' tab.";
//            return false;
//        }
//        
//        if(getData("Select Stackeholders").equalsIgnoreCase("YES")){
//            //Select *
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Stakeholders_SelectAll())){
//                error = "Failed to wait for 'Select All' Stakeholders.";
//                return false;
//            }
//            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Stakeholders_SelectAll())){
//                error = "Failed to click on 'Select All' Stakeholders..";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully clicked all.");
//        }else{
//            //Select any
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyStakeholders(getData("Stakeholder Name")))){
//                error = "Failed to wait for '" + getData("Stakeholder Name") + "' field.";
//                return false;
//            }
//            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyStakeholders(getData("Stakeholder Name")))){
//                error = "Failed to wait for '" + getData("Stakeholder Name") + "' field.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Stakeholder Name : '" + getData("Stakeholder Name") + "'.");
//        }
//        
//        //Entities
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.entitiesTab())){
//            error = "Failed to wait for 'Enties' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.entitiesTab())){
//            error = "Failed to click on 'Enties' tab.";
//            return false;
//        }
//        
//        List<WebElement> stakeholders = SeleniumDriverInstance.Driver.findElements(By.xpath(Stakeholder_Entity_PageObjects.stakeholder_Array(getData("Entity name"))));
//            
//        int counter = stakeholders.size();
//
//        pause(3000);
//        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.anyStakeholders2(getData("Entity name"), counter))){
//            error = "Failed to scroll for '" + getData("Entity name") + "' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyStakeholders2(getData("Entity name"), counter), 10000)){
//            error = "Failed to wait for '" + getData("Entity name") + "' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyStakeholders2(getData("Entity name"), counter))){
//            error = "Failed to wait for '" + getData("Entity name") + "' field.";
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.entities(getData("Entity name")))){
//            error = "Failed to wait for 'Enties' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.entities(getData("Entity name")))){
//            error = "Failed to click on '" + getData("Entity name") + "' tab.";
//            return false;
//        }
//         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.entitiesCloseDropdown())){
//            error = "Failed to click on 'Enties' tab.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Entity Name : '" + getData("Entity name") + "'.");
//        
//        

        //Engagement purpose
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementPurposeTab())){
            error = "Failed to wait for 'Engagement Purpose' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.engagementPurposeTab())){
            error = "Failed to click on 'Engagement Purpose' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyEngagementPurpose(getData("Engagement Purpose")))){
            error = "Failed to wait for '" + getData("Engagement Purpose") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyEngagementPurpose(getData("Engagement Purpose")))){
            error = "Failed to click on '" + getData("Engagement Purpose") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Purpose : '" + getData("Engagement Purpose") + "'.");
        
        //Engagement method
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementMethodTab())){
            error = "Failed to wait for 'Engagement method' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.engagementMethodTab())){
            error = "Failed to click on 'Engagement method' tab.";
            return false;
        }
        //Expand In-Person engagements
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.expandInPerson())){
            error = "Failed to wait for 'In-person engagements' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.expandInPerson())){
            error = "Failed to click on 'In-person engagements' option.";
            return false;
        }
        //Any anyEngagement
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyEngagementMethod(getData("Engagement Method")))){
            error = "Failed to wait for '" + getData("Engagement Method") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyEngagementMethod(getData("Engagement Method")))){
            error = "Failed to click on '" + getData("Engagement Method") + "' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Method : '" + getData("Engagement Method") + "'.");
        
        
         //Responsible person
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.responsiblePersonTab())){
           error = "Failed to wait for 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.responsiblePersonTab())){
           error = "Failed to click on 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to wait for '" + getData("Responsible Person") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to click on '" + getData("Responsible Person") + "' option.";
           return false;
       }
       
            // Impact Types
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.impacttypes_selectall())){
            error = "Failed to wait for Stakeholder Individual Impact Types.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.impacttypes_selectall())){
            error = "Failed to click Stakeholder Individual Impact Types select all.";
            return false;
        }
        
        //Contact Enquiry / topic
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contactEnquiryOrTopic())){
           error = "Failed to wait for 'Contact Enquiry / topic' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contactEnquiryOrTopic())){
           error = "Failed to click on 'Contact Enquiry / topic' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contactEnquiryOrTopicValue(getData("Contact Enquiry / topic")))){
           error = "Failed to wait for '" + getData("Contact Enquiry / topic") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contactEnquiryOrTopicValue(getData("Contact Enquiry / topic")))){
           error = "Failed to click on '" + getData("Contact Enquiry / topic") + "' option.";
           return false;
       }
       
       //Location
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.location())){
           error = "Failed to wait for 'Location' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.location())){
           error = "Failed to click on 'Location' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.locationValue(getData("Location")))){
           error = "Failed to wait for '" + getData("Location") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.locationValue(getData("Location")))){
           error = "Failed to click on '" + getData("Location") + "' option.";
           return false;
       }
       
        //Engagement description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementDescription())){
            error = "Failed to wait for 'Engagement Description' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.engagementDescription(), getData("Engagement Description"))){
            error = "Failed to '" + getData("Engagement Description") + "' into Engagement Description field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement description : '" + getData("Engagement Description") + "'.");
        
//        //Engagement Plan
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementPlan())){
//            error = "Failed to wait for 'Engagement Plan' field";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.engagementPlan(), getData("Engagement Plan"))){
//            error = "Failed to '" + getData("Engagement Plan") + "' into Engagement Plan field";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Engagement Plan : '" + getData("Engagement Plan") + "'.");
//        
//        if(getData("Management Approval").equalsIgnoreCase("YES")){   
//            //ManagementApprovalAction
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.managementApprovalActionCheckBox())){
//                error = "Failed to wait for 'Management Approval Action' checkbox.";
//                return false;
//            }
//            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.managementApprovalActionCheckBox())){
//                error = "Failed to click on 'Management Approval Action' checkbox.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Management Approval Action' checkbox.");
//
//            //StakeholderApprovalAction
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.stakeholderApprovalAction())){
//                error = "Failed to wait for 'Stakeholder Approval Action' checkbox.";
//                return false;
//            }
//            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.stakeholderApprovalAction())){
//                error = "Failed to click on 'Stakeholder Approval Action' checkbox.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Stakeholder Approval Action' checkbox.");
//        }
//        
//        //Number of attendences
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.numberOfAttendees())){
//            error = "Failed to wait for 'Number of attendees' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpathUsingActions(Stakeholder_Entity_PageObjects.numberOfAttendees(), getData("Number of attendees"))){
//            error = "Failed to enter '" + getData("Number of attendees") + "' into 'Number of attendees' field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Number of attendees : '" + getData("Number of attendees") + "'.");
//        
//       //Responsible person
//       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.responsiblePersonTab())){
//           error = "Failed to wait for 'Responsible Person' tab.";
//           return false;
//       }
//       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.responsiblePersonTab())){
//           error = "Failed to click on 'Responsible Person' tab.";
//           return false;
//       }
//       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
//           error = "Failed to wait for '" + getData("Responsible Person") + "' option.";
//           return false;
//       }
//       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
//           error = "Failed to click on '" + getData("Responsible Person") + "' option.";
//           return false;
//       }
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Engagement_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Engagement_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        pause(10000);
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
//        if(SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup())){
//            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup());
//
//            if(saved.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//            }else{   
//                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.failed())){
//                    error = "Failed to wait for error message.";
//                    return false;
//                }
//
//                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.failed());
//
//                if(failed.equals("ERROR: Record could not be saved")){
//                    error = "Failed to save record.";
//                    return false;
//                }
//            }
//        }
        
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        
        //Switch to a new tab or window
        if(!SeleniumDriverInstance.switchToWindow(SeleniumDriverInstance.Driver, Stakeholder_Entity_PageObjects.projectTitle())){
            error = "Failed to switch to 'IsoMetrix' window.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully switched to 'IsoMetrix' window.");
        
        
        return true;
    }
    
//    public boolean EngagementWindow(){
//        //Search Tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchTab(), 10000)){
//            error = "Failed to wait for 'Search' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchTab())){
//            error = "Failed to click on 'Search' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchButton())){
//            error = "Failed to wait for 'Search' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchButton())){
//            error = "Failed to click on 'Search' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementsGridView())){
//            error = "Failed to wait for 'Engagements' grid view.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully 'Search' button clicked.");
//        
//        return true;
//    }

}
