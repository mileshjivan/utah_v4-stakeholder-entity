/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "View Related Incidents - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_View_Related_Incidents_Main_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR5_View_Related_Incidents_Main_Scenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntity(){
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(12000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        //Entity name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ri_ename())){
            error = "Failed to wait for 'Entity name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.ri_ename(), getData("Incident"))){
            error = "Failed to click on 'Entity name' input field.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Entity name' input field.");

        //Search button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ri_esearch())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ri_esearch())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ri_eselect(testData.getData("Incident")))){
            error = "Failed to wait for 'Related Incident Record' from grid.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ri_eselect(testData.getData("Incident")))){
            error = "Failed to select 'Related Incident Record' from grid.";
            return false;
        }
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
         //Process flow 
         pause(12000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        for (int i = 0; i < 5; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
           
        //Related Incidents tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ri_related())){
            error = "Failed to wait for 'Related Incidents' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ri_related())){
            error = "Failed to click 'Related Incidents' tab.";
            return false;
        }
        
        pause(5000);
        //Scroll up
        JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
         js.executeScript("window.scrollBy(0,-1000)");
        narrator.stepPassedWithScreenShot("Successfully showing list of Related Incidents");
        //Related Incident Record select
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ri_record(), 50000)){
//            error = "Failed to wait for 'Related Incident Record' from grid.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ri_record())){
//            error = "Failed to select 'Related Incident Record' from grid.";
//            return false;
//        }
//
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ri_wait())){
//            error = "Failed to wait for 'Incident Management' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ri_wait())){
//            error = "Failed to select 'Incident Management' tab.";
//            return false;
//        }
//        pause(5000);
        return true;
    }

}
