/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Related Stakeholder - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Related_Stakeholder_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR2_Capture_Related_Stakeholder_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!createNewIndividual()){
            return narrator.testFailed("Failed due - " + error);
        }
//        if(!addNewStakeholder()){
//            return narrator.testFailed("Failed due - " + error);
//        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntity(){    
        //Get Parent WindowHandle
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //Navigate to Members Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.members_tab())){
            error = "Failed to wait for 'Members Tab' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.members_tab())){
            error = "Failed to click on 'Members Tab' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Members' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_createnewindividual())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_createnewindividual())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean createNewIndividual(){
        //Switch to new tab
        pause(5000);
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
         pause(25000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_processflow(), 5000)){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        //Stakeholder Individual first name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_fname())){
            error = "Failed to wait for 'Stakeholder Individual first name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.si_fname(), testData.getData("First name"))){
            error = "Failed to click 'Stakeholder Individual first name' input field.";
            return false;
        } 
        //Stakeholder Individual last name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_lname())){
            error = "Failed to wait for 'Stakeholder Individual last name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.si_lname(), testData.getData("Last name"))){
            error = "Failed to click 'Stakeholder Individual last name' input field.";
            return false;
        } 
        //Stakeholder Individual known as
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_knownas())){
            error = "Failed to wait for 'Stakeholder Individual Known as' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.si_knownas(), testData.getData("Known as"))){
            error = "Failed to click 'Stakeholder Individual Known as' input field.";
            return false;
        } 
        //Stakeholder Individual title dropdown
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_title_dropdown())){
            error = "Failed to wait for 'Stakeholder title' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_title_dropdown())){
            error = "Failed to click 'Stakeholder title' dropdown.";
            return false;
        }
        //Stakeholder Individual title select 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to wait for 'Stakeholder title' options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to select '"+testData.getData("Title")+"' from 'Title' options.";
            return false;
        }
        
         
        //Relationship owner
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        //Relationship owner select 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_relationshipOwner_dropdownValue(testData.getData("Relationship owner")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_relationshipOwner_dropdownValue(testData.getData("Relationship owner")))){
            error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' options.";
            return false;
        }
        
 
//        //Profile tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.profile_tab())){
//            error = "Failed to wait for Profile tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.profile_tab())){
//            error = "Failed to click Profile tab.";
//            return false;
//        }
        
        //Stakeholder categories select all
         
        //Stakeholder Individual categorie 1
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        
       //Stakeholder Individual Business Unit
        if(!testData.getData("Business Unit").equals("Global Company")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_appbu_expand("Global Company"))){
                error = "Failed to wait to expand 'Global Company'.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_appbu_expand("Global Company"))){
                error = "Failed to expand 'Global Company'.";
                return false;
            }
            if(!testData.getData("Business Unit").equals("South Africa")){
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_appbu_expand("South Africa"))){
                    error = "Failed to wait to expand 'South Africa'.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_appbu_expand("South Africa"))){
                    error = "Failed to expand 'South Africa'.";
                    return false;
                }
                if(!testData.getData("Business Unit").equals("Victory Site")){
                    error = "Failed to find Business Unit";
                    return false;
                }else
                {
                    if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_appbu(testData.getData("Business Unit")))){
                        error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                    if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_appbu(testData.getData("Business Unit")))){
                        error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                }
            }else
            {
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_appbu(testData.getData("Business Unit")))){
                    error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_appbu(testData.getData("Business Unit")))){
                    error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
            }
        }else
        {
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_appbu(testData.getData("Business Unit")))){
                error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_appbu(testData.getData("Business Unit")))){
                error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
        }
        //Stakeholder Individual Impact Types
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_impacttypes_selectall())){
            error = "Failed to wait for Stakeholder Individual Impact Types.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_impacttypes_selectall())){
            error = "Failed to click Stakeholder Individual Impact Types select all.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Profile details entered.");
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        
        pause(15000);
        
        narrator.stepPassedWithScreenShot("Stakeholder Details, Stakeholder Analysis, Relationships, Entities, Vulnerability, Initiatives, Commitments, Resettlement, Land Access, Related Assessment, Engagements, Actions and Supporting Documents tabs are displayed. ");
        
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
         //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          
        return true;
    }

}

    
//    public boolean addNewStakeholder(){
//        //switch to the iframe
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        //Add button
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.member_add())){
//            error = "Failed to wait for 'Add' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.member_add())){
//            error = "Failed to click on 'Add' button.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
//        
//        FR2_Capture_Related_Stakeholder_MainScenario addNewStakeholder = new FR2_Capture_Related_Stakeholder_MainScenario();      
//        addNewStakeholder.enterDetails();
//        
//        return true;
//    }
//
//}
