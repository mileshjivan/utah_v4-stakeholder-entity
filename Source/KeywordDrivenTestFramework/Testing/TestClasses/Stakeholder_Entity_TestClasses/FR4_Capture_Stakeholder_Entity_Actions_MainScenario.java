/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Entity Actions - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Stakeholder_Entity_Actions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR4_Capture_Stakeholder_Entity_Actions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
//        if(!enterDetails()){
//            return narrator.testFailed("Failed due - " + error);
//        }

        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
         return narrator.finalizeTest("Successfully navigated ");
    }

    
    
    //Enter data
//    public boolean enterDetails(){
//         //This clicks Entity Information tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.entityInformationTab())){
//            error = "Failed to wait for 'Add' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.entityInformationTab())){
//            error = "Failed to click on 'Add' button.";
//            return false;
//        }
//        
//        //Primary contact number 
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_pcn())){
//            error = "Failed to wait for 'Primary contact number' input field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.actions_pcn(), testData.getData("Primary contact number"))){
//            error = "Failed to enter '"+testData.getData("Primary contact number")+"' into 'Primary contact number' input field.";
//            return false;
//        }
//        //Secondary contact number 
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_scn())){
//            error = "Failed to wait for 'Primary contact number' input field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.actions_scn(), testData.getData("Secondary contact number"))){
//            error = "Failed to enter '"+testData.getData("Secondary contact number")+"' into 'Secondary contact number' input field.";
//            return false;
//        }
//        //Address
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_address())){
//            error = "Failed to wait for 'Address' textarea.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.actions_address(), testData.getData("Address"))){
//            error = "Failed to enter '"+testData.getData("Address")+"' into 'Address' textarea.";
//            return false;
//        }
//        //Correspondance Address
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_caddress())){
//            error = "Failed to wait for 'Correspondance Address' textarea.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.actions_caddress(), testData.getData("Correspondance Address"))){
//            error = "Failed to enter '"+testData.getData("Correspondance Address")+"' into 'Correspondance Address' textarea.";
//            return false;
//        }
//        //Website
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_website())){
//            error = "Failed to wait for 'Website' input field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.actions_website(), testData.getData("Website"))){
//            error = "Failed to enter '"+testData.getData("Website")+"' into 'Website' input field.";
//            return false;
//        }
//             return true;
//    }
        
        
        public boolean navigateToStakeholderEntity(){   
          //Click right chevron arrow
//          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
//            error = "Failed to wait for 'Actions' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
//            error = "Failed to click on 'Actions' tab.";
//            return false;
//        }

          if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        for (int i = 0; i < 6; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowTab())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
            
        //Navigate to Actions Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_tab())){
            error = "Failed to wait for 'Actions' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.actions_tab())){
            error = "Failed to click on 'Actions' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Actions' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.actions_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

         //Enter Action description
         pause(5000);
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.action_description())){
            error = "Failed to wait for 'Action description' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.action_description(), testData.getData("Action description"))){
            error = "Failed to enter '"+testData.getData("Website")+"' into 'Action description' input field.";
            return false;
        }
         
        //Select Department responsible
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.departmentResponsibleDropDown())){
           error = "Failed to wait for 'Department responsible'";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.departmentResponsibleDropDown())){
           error = "Failed to click on 'Department responsible'";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.departmentResponsibleDropDownValue(getData("Department responsible")))){
           error = "Failed to wait for '" + getData("Department responsible") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.departmentResponsibleDropDownValue(getData("Department responsible")))){
           error = "Failed to click on '" + getData("Department responsible") + "' option.";
           return false;
       }
       
        //Select Responsible person
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actionsresponsiblePersonTab())){
           error = "Failed to wait for 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.actionsresponsiblePersonTab())){
           error = "Failed to click on 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actionsanyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to wait for '" + getData("Responsible Person") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.actionsanyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to click on '" + getData("Responsible Person") + "' option.";
           return false;
       }
        
       //Action due date
        String ActionDueDate=SeleniumDriverInstance.getADate("15-10-2020", 0);
         
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.actionDueDate(), ActionDueDate))
        {
            error = "Failed to enter  Action due date";
            return false;
        }
        
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actionDueDate())){
//            error = "Failed to wait for 'Action due date' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.actionDueDate(), getData("Action due date"))){
//            error = "Failed to enter '" + getData("Action due date")+ "' into 'Action due date' field.";
//            return false;
//        }
    //    narrator.stepPassedWithScreenShot("Action due date : '" + getData("Action due date")+ "'.");
         
        //Save Actions
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.actions_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.actions_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        pause(10000);
        narrator.stepPassedWithScreenShot("Successfully saved actions");
      
        //Check if the record has been Saved
//        if(SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup())){
//            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup());
//
//            if(saved.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//            }else{   
//                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.failed())){
//                    error = "Failed to wait for error message.";
//                    return false;
//                }
//
//                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.failed());
//
//                if(failed.equals("ERROR: Record could not be saved")){
//                    error = "Failed to save record.";
//                    return false;
//                }
//            }
//        }
        
         //Save Stakeholder Entity
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.StakeholderEntity_Save())){
//            error = "Failed to wait for 'Save' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.StakeholderEntity_Save())){
//            error = "Failed to click on 'Save' button.";
//            return false;
//        }
//        //Saving mask
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }
      
        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup1())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        String recordsaved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup1());
//        
//        if(recordsaved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
        
        
        return true;
    }

}
