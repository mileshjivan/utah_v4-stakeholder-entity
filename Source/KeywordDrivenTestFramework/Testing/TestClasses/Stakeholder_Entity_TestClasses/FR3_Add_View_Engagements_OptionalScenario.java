/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add_View Engagements - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR3_Add_View_Engagements_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Add_View_Engagements_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if(!EngagementWindow()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }
    public boolean EngagementWindow(){
        //Click on Engagement viewing grid
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.engagementsGridView())){
            error = "Failed to wait for 'Engagements' grid view.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.engagementsGridView())){
            error = "Failed to click on 'Engagements' grid view.";
            return false;
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully click 'Engagements' grid view.");
        
        return true;
    }

}
