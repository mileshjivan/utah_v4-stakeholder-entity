/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsoMetricsIncidentPageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "View Reports - Main Scenario",
        createNewBrowserInstance = false
)

public class FR9_View_Reports_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR9_View_Reports_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");     
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntity(){
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }
        SeleniumDriverInstance.pause(2000);
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");

        //Search button
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ri_esearch())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ri_esearch())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Reports
        pause(10000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.report())){
            error = "Failed to wait for 'report' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.report())){
            error = "Failed to click on 'report' button.";
            return false;
        }
        //View Reports
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.view_reports())){
            error = "Failed to wait for 'view reports' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.view_reports())){
            error = "Failed to click 'view reports' button.";
            return false;
        }
        //Wait View Reports
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to pop-up.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.popup_conf())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.popup_conf())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Continue' button.");
        
        if(!SeleniumDriverInstance.switchToWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.view_wait(), 50000)){
            error = "Failed to wait for 'View Report' page.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully View Reports.");
        pause(2000);
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
         //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.swithToFrameByName("ifrMain");
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }
        
        //Full Report
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.full_Reports())){
            error = "Failed to wait for 'Full Reports' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.full_Reports())){
            error = "Failed to select 'Full Reports' button.";
            return false;
        }
        //Wait View Reports
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to pop-up.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.popup_conf())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.popup_conf())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Continue' button.");
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.full_wait(), 50000)){
            error = "Failed to wait for 'Full Reports' page.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Viewed Full Report button.");
        pause(5000);
          //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        System.out.println(SeleniumDriverInstance.Driver.getTitle());

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.swithToFrameByName("ifrMain");
        
        return true;
    }

}
