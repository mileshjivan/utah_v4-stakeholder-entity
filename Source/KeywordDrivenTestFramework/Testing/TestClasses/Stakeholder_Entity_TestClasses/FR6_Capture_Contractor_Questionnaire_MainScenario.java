/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Contractor Questionnaire - Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_Capture_Contractor_Questionnaire_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR6_Capture_Contractor_Questionnaire_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToContractorSupplierManager()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!companyInformation()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!businessPartnerCompany()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!companyCertificates()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!safetyHealthLegalRequirements()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!healthSafetyEnvironmentCommunity()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!additionalCompanyInformation()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!businessPartnerRiskRanking()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToContractorSupplierManager(){    
        //Navigate to Contractor Or Supplier Manager Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractorSupplierManager_Tab())){
            error = "Failed to wait for 'Contractor Or Supplier Manager' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contractorSupplierManager_Tab())){
            error = "Failed to click on 'Contractor Or Supplier Manager' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Contractor Or Supplier Manager' tab.");
        
        //Navigate to Questionnaires panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.questionnaires_Panel())){
            error = "Failed to wait for 'Questionnaires' panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.questionnaires_Panel())){
            error = "Failed to click on 'Questionnaires' panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Contractor Or Supplier Manager' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.questionnaires_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.questionnaires_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process Flow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.questionnaireProcessFlow())) {
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.questionnaireProcessFlow())) {
            error = "Failed to click on 'Process Flow' button field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process Flow' button.");
        
        return true;
    }
    
    public boolean companyInformation(){
        //Navigate to Company Information
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.companyInformationTab())){
            error = "Failed to wait for 'Company Information' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.companyInformationTab())){
            error = "Failed to click on 'Company Information' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Company Information' Tab.");
        
        //Company Type //Close Corporation (CC)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.companyType_Dropdown())){
            error = "Failed to wait for 'Company Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.companyType_Dropdown())){
            error = "Failed to click on 'Company Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.companyType_Option(getData("Company Type")))){
            error = "Failed to wait for '" + getData("Company Type") + "' from 'Company Type' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.companyType_Option(getData("Company Type")))){
            error = "Failed to click on '" + getData("Company Type") + "' into 'Company Type' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Company Type: '" + getData("Company Type") + "'.");
        
        //Relevant entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.relevantEntity_RegNo())){
            error = "Failed to wait for 'Relevant entity' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.relevantEntity_RegNo(), getData("Relevant Entity"))){
            error = "Failed to enter '" + getData("Relevant Entity") + "' into 'Relevant entity' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Relevant entity Reg No: '" + getData("Relevant Entity") + "'.");
        
        return true;
    }
    public boolean businessPartnerCompany(){
        //Navigate to (1) BUSINESS PARTNER COMPANY ENGAGEMENT FORM
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.businessPartnerTab())){
            error = "Failed to wait for '(1) BUSINESS PARTNER COMPANY ENGAGEMENT FORM' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.businessPartnerTab())){
            error = "Failed to click on '(1) BUSINESS PARTNER COMPANY ENGAGEMENT FORM' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to '(1) BUSINESS PARTNER COMPANY ENGAGEMENT FORM' Tab.");
        
        //Main Area 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.mainArea())){
            error = "Failed to wait for 'Main Area' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.mainArea(), getData("Main Area"))){
            error = "Failed to enter '" + getData("Main Area") + "' into 'Main Area' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Main Area: '" + getData("Main Area") + "'.");
        
        //Operating Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.operatingUnit())){
            error = "Failed to wait for 'Operating Unit' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.operatingUnit(), getData("Operating Unit"))){
            error = "Failed to enter '" + getData("Operating Unit") + "' into 'Operating Unit' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Operating Unit: '" + getData("Operating Unit") + "'.");
        
        return true;
    }
    public boolean companyCertificates(){
        //Navigate to Company Certificates
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.companyCertificatesTab())){
            error = "Failed to wait for 'Company Certificates' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.companyCertificatesTab())){
            error = "Failed to click on 'Company Certificates' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Company Certificates' Tab.");
        
        //Quality Management
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.qualityManagement_Dropdown())){
            error = "Failed to wait for 'Quality Management' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.qualityManagement_Dropdown())){
            error = "Failed to click on 'Quality Management' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.qualityManagement_Option(getData("Quality Management")))){
            error = "Failed to wait for '" + getData("Quality Management") + "' from 'Quality Management' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.qualityManagement_Option(getData("Quality Management")))){
            error = "Failed to click on '" + getData("Quality Management") + "' into 'Quality Management' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Quality Management: '" + getData("Quality Management") + "'.");
        
        //Environmental Management
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.environmentalManagement_Dropdown())){
            error = "Failed to wait for 'Environmental Management' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.environmentalManagement_Dropdown())){
            error = "Failed to click on 'Environmental Management' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.environmentalManagement_Option(getData("Environmental Management")))){
            error = "Failed to wait for '" + getData("Environmental Management") + "' from 'Environmental Management' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.environmentalManagement_Option(getData("Environmental Management")))){
            error = "Failed to click on '" + getData("Environmental Management") + "' into 'Environmental Management' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Environmental Management: '" + getData("Environmental Management") + "'.");
        
        return true;
    }
    public boolean safetyHealthLegalRequirements(){
        //Navigate to Safety and Health Legal Requirements & Issues
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.safetyHealthIssuesTab())){
            error = "Failed to wait for 'Safety and Health Legal Requirements & Issues' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.safetyHealthIssuesTab())){
            error = "Failed to click on 'Safety and Health Legal Requirements & Issues' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Safety and Health Legal Requirements & Issues' Tab.");
        
        //Are you registered with the Department of Labour in terms of the COID Act
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.COID_Act_Dropdown())){
            error = "Failed to wait for 'COID Act' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.COID_Act_Dropdown())){
            error = "Failed to click on 'COID Act' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.COID_Act_Option(getData("COID Act")))){
            error = "Failed to wait for '" + getData("COID Act") + "' from 'COID Act' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.COID_Act_Option(getData("COID Act")))){
            error = "Failed to click on '" + getData("COID Act") + "' into 'COID Act' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("COID Act: '" + getData("COID Act") + "'.");
        
        // Label  Provide your COID (Compensation for Occupational Injuries and Diseases) Registration Number as it appears on your Letter of Good Standing.
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.COID_RegNo())){
            error = "Failed to wait for 'COID Registration Number' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.COID_RegNo(), getData("COID Registration Number"))){
            error = "Failed to enter '" + getData("COID Registration Number") + "' into 'COID Registration Number' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("COID Registration Number: '" + getData("COID Registration Number") + "'.");
                
        return true;
    }
    public boolean healthSafetyEnvironmentCommunity(){
        //Right Arrow button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rightArrowButton())){
            error = "Failed to wait for 'Right Arrow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowButton())){
            error = "Failed to double click on 'Right Arrow' button.";
            return false;
        }
        
        //Navigate to Health,Safety,Environmental and Community (Policies, Plans & Systems)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.healthSafeyEnvironmentTab())){
            error = "Failed to wait for 'Health,Safety,Environmental and Community (Policies, Plans & Systems)' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.healthSafeyEnvironmentTab())){
            error = "Failed to click on 'Health,Safety,Environmental and Community (Policies, Plans & Systems)' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Health,Safety,Environmental and Community (Policies, Plans & Systems)' Tab.");
        
        //Legal Register
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.legalRegister_Dropdown())){
            error = "Failed to wait for 'Legal Register' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.legalRegister_Dropdown())){
            error = "Failed to click on 'Legal Register' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.legalRegister_Option(getData("Legal Register")))){
            error = "Failed to wait for '" + getData("Legal Register") + "' from 'Legal Register' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.legalRegister_Option(getData("Legal Register")))){
            error = "Failed to click on '" + getData("Legal Register") + "' into 'Legal Register' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Legal Register: '" + getData("Legal Register") + "'.");
        
        //Health and Safety Plan
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.healthSafetyPlan_Dropdown())){
            error = "Failed to wait for 'Health and Safety Plan' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.healthSafetyPlan_Dropdown())){
            error = "Failed to click on 'Health and Safety Plan' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.healthSafetyPlan_Option(getData("Health and Safety Plan")))){
            error = "Failed to wait for '" + getData("Health and Safety Plan") + "' from 'Health and Safety Plan' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.healthSafetyPlan_Option(getData("Health and Safety Plan")))){
            error = "Failed to click on '" + getData("Health and Safety Plan") + "' into 'Health and Safety Plan' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Health and Safety Plan: '" + getData("Health and Safety Plan") + "'.");
        
        return true;
    }
    public boolean additionalCompanyInformation(){
        //Right Arrow button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rightArrowButton())){
            error = "Failed to wait for 'Right Arrow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowButton())){
            error = "Failed to double click on 'Right Arrow' button.";
            return false;
        }
        
        //Navigate to Additional Company Information
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.additionalCompanyInformationTab())){
            error = "Failed to wait for 'Additional Company Information' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.additionalCompanyInformationTab())){
            error = "Failed to click on 'Additional Company Information' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Additional Company Information' Tab.");
        
        //Is the premises you are using Rented, Owned or Leased?
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rentedOwnedLeased_Dropdown())){
            error = "Failed to wait for 'Rented, Owned or Leased?' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.rentedOwnedLeased_Dropdown())){
            error = "Failed to click on 'Rented, Owned or Leased?' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rentedOwnedLeased_Option(getData("Rented Owned Leased")))){
            error = "Failed to wait for '" + getData("Rented Owned Leased") + "' from 'Rented, Owned or Leased?' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.rentedOwnedLeased_Option(getData("Rented Owned Leased")))){
            error = "Failed to click on '" + getData("Rented Owned Leased") + "' into 'Rented, Owned or Leased?' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Rented, Owned or Leased?: '" + getData("Rented Owned Leased") + "'.");
        
        //Furnish Annual Nett Profit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.furnishAnnualNettProfit())){
            error = "Failed to wait for 'Furnish Annual Nett Profit' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.furnishAnnualNettProfit(), getData("Furnish Annual Nett Profit"))){
            error = "Failed to enter '" + getData("Furnish Annual Nett Profit") + "' into 'Furnish Annual Nett Profit' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Furnish Annual Nett Profit: '" + getData("Furnish Annual Nett Profit") + "'.");
        
        return true;
    }
    public boolean businessPartnerRiskRanking(){
        //Right Arrow button
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.rightArrowButton())){
//            error = "Failed to wait for 'Right Arrow' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.rightArrowButton())){
//            error = "Failed to double click on 'Right Arrow' button.";
//            return false;
//        }
//        
//        //Navigate to (2) BUSINESS PARTNER RISK RANKING
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.businessPartnerRiskRankingTab())){
//            error = "Failed to wait for '(2) BUSINESS PARTNER RISK RANKING' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.businessPartnerRiskRankingTab())){
//            error = "Failed to click on '(2) BUSINESS PARTNER RISK RANKING' tab.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully navigated to '(2) BUSINESS PARTNER RISK RANKING' Tab.");
//        
//        //Scope of work
//         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.scopeOfWork())){
//            error = "Failed to wait for 'Scope of Work' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.scopeOfWork(), getData("Scope of Work"))){
//            error = "Failed to enter '" + getData("Scope of Work") + "' into 'Scope of Work' field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Scope of Work: '" + getData("Scope of Work") + "'.");
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.questionnaire_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.questionnaire_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
         
        pause(10000);
        narrator.stepPassedWithScreenShot("Sucessfully saved");
        
        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup())){
//            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup());
//
//            if(saved.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//            }else{   
//                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.failed())){
//                    error = "Failed to wait for error message.";
//                    return false;
//                }
//
//                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.failed());
//
//                if(failed.equals("ERROR: Record could not be saved")){
//                    error = "Failed to save record.";
//                    return false;
//                }
//            }
//        }
        

        
        return true;
    }
}
