/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Entity Information - Main Scenario",
        createNewBrowserInstance = false
)

public class UC_STE_01_02_StakeholderEntityAdditionalDetails_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_STE_01_02_StakeholderEntityAdditionalDetails_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToEntityInformation())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToEntityInformation(){
        //Left Arrow
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to wait for 'Left' Arrow.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to click on 'Left' Arrow.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
        
        //Navigate to Stakeholder Details Tab
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.entityInformationTab())){
            error = "Failed to wait for 'Stakeholder Details' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.entityInformationTab())){
            error = "Failed to click on 'Stakeholder Details' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Entity Information' tab.");
        
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Phone number
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.primaryContactNo())){
            error = "Failed to wait for 'Phone number' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.primaryContactNo(), getData("Phone number"))){
            error = "Failed to enter '" + getData("Phone number") + "' into 'Primary contact number' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Phone number: '" + getData("Primary Contact No") + "' .");
        
        //Secondary contact number
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.secondaryContactNo())){
//            error = "Failed to wait for 'Secondary contact number' input.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.secondaryContactNo(), getData("Secondary Contact No"))){
//            error = "Failed to enter '" + getData("Secondary Contact No") + "' into 'Secondary contact number' input.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Secondary contact number: '" + getData("Secondary Contact No") + "' .");
        

        //Email Address
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.emailAddress())){
            error = "Failed to wait for 'Email Address' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.emailAddress(), getData("Email Address"))){
            error = "Failed to enter '" + getData("Email Address") + "' into 'Email Address' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Email Address : '" + getData("Email Address") + "' .");
        
        // Address
        //Street 1
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.streetOne())){
            error = "Failed to wait for 'Street 1' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.streetOne(), getData("Street 1"))){
            error = "Failed to enter '" + getData("Street 1") + "' into 'Street 1' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Street 1 : '" + getData("Street 1") + "' .");
        
         //Street 2
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.streetTwo())){
            error = "Failed to wait for 'Street 2' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.streetTwo(), getData("Street 2"))){
            error = "Failed to enter '" + getData("Street 2") + "' into 'Street 2' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Street 2 : '" + getData("Street 2") + "' .");
        
        //Zip /Postal code
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_PostalCode())){
            error = "Failed to wait for 'Postal code' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.se_PostalCode(), testData.getData("Postal code"))){
            error = "Failed to click 'Postal code' input field.";
            return false;
        } 
        
        //Location
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_Location())){
            error = "Failed to wait for 'Location' input field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_Location())){
            error = "Failed to click  'Location' input field.";
            return false;
        }
        
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_Location_select(testData.getData("Location")))){
            error = "Failed to wait for Location dropdown option.";
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_Location_select(testData.getData("Location")))){
            error = "Failed to click Location dropdown option.";
            return false;
        }
         
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
         
       //Status dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_Status())){
            error = "Failed to wait for 'Status' input field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_Status())){
            error = "Failed to click  'Status' input field.";
            return false;
        }
        
       
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_Status_select(testData.getData("Status")))){
            error = "Failed to wait for Status dropdown option.";
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_Status_select(testData.getData("Status")))){
            error = "Failed to click Status dropdown option.";
            return false;
        }
         
         //Main Contact
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.mainContact())){
            error = "Failed to wait for 'Main Contact' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.mainContact(), getData("Main Contact"))){
            error = "Failed to enter '" + getData("Main Contact") + "' into 'Main Contact' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Main Contact: '" + getData("Main Contact") + "' .");
        
        //Main Contact Phone
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.mainContactPhone())){
            error = "Failed to wait for 'Main Contact Phone' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.mainContactPhone(), getData("Main Contact Phone"))){
            error = "Failed to enter '" + getData("Main Contact Phone") + "' into 'Main Contact Phone' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Main Contact Phone: '" + getData("Main Contact Phone") + "' .");
        
        //Main Contact Email
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.mainContactEmail())){
            error = "Failed to wait for 'Main Contact Email' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.mainContactEmail(), getData("Main Contact Email"))){
            error = "Failed to enter '" + getData("Main Contact Email") + "' into 'Main Contact Email' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Main Contact Email: '" + getData("Main Contact Email") + "' .");
        
        //Main Contact Individual dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_MainContactIndividual())){
            error = "Failed to wait for 'Main Contact Individual'   field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_MainContactIndividual())){
            error = "Failed to click  'Main Contact Individual'  field.";
            return false;
        }
        
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_MainContactIndividual_select(testData.getData("Main Contact Individual")))){
            error = "Failed to wait for Main Contact Individual dropdown option.";
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_MainContactIndividual_select(testData.getData("Main Contact Individual")))){
            error = "Failed to click Main Contact Individual dropdown option.";
            return false;
        }
         
        //Correspondence address
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.correspondenceAddress())){
//            error = "Failed to wait for 'Correspondence Address' input.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.correspondenceAddress(), getData("Correspondence Address"))){
//            error = "Failed to enter '" + getData("Correspondence Address") + "' into 'Correspondence Address' textarea.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Correspondence Address : '" + getData("Correspondence Address") + "' .");
//        
//        //Website
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.website())){
//            error = "Failed to wait for 'Website' input.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.website(), getData("Website"))){
//            error = "Failed to enter '" + getData("Website") + "' into 'Website' input.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Website : '" + getData("Website") + "'.");
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
//        if(SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup())){
//            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup());
//
//            if(saved.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//            }else{   
//                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.failed())){
//                    error = "Failed to wait for error message.";
//                    return false;
//                }
//
//                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.failed());
//
//                if(failed.equals("ERROR: Record could not be saved")){
//                    error = "Failed to save record.";
//                    return false;
//                }
//            }
//        }
        

        
        return true;
    }

}
