/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author vijaya
 */
public class TrainingSiteSignInPageObjects extends BaseClass
{
 
    public static String url()
    {
        return "https://usazu-pr01.isometrix.net/IsoMetrix.UK.V4.Training/login.aspx?signin=1";
    }
       
    public static String userNameTextXpath()
    {
        return ".//input[@id='txtUsername']";
    }
    
    public static String passwordTextXpath()
    {
        return ".//input[@id='txtPassword']";
    }
    
    public static String signInButtontXpath()
    {
        return ".//div[@id='btnLoginSubmit']";
    }
    
    
}
