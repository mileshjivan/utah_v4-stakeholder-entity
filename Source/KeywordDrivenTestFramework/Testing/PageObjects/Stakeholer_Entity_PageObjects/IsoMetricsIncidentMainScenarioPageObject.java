/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects;

/**
 *
 * @author MJivan
 */
public class IsoMetricsIncidentMainScenarioPageObject {

    public static String environmentalHealthSafetyXpath() {
        return "//label[contains(text(),'Environmental, Health')]";
    }
    
    public static String incidentManagementreload(){
        return "//div[@id='control_F3EBE4A9-5055-414F-8D34-1DD0E968BD92']//b[@original-title='Refresh items list']";
    }
    
    public static String exeternalPartiesText(){
        return "//div[@id='control_C7FC396F-66BC-49D6-848A-2659CFADAE7D']";
    }

    public static String environmentalHealth() {
        return "//div[@id='section_711036e0-fe8c-478f-a260-87beae7432fc']";
    }

    public static String addDetailsXpath() {
        return "//div[text()='2.Verification and Additional Detail']";
    }

    public static String IncidentManagmentXpath() {
        return "//label[contains(text(),'Incident Management')]";
    }

    public static String IncidentsManagment() {
        return "//label[contains(text(),'Incidents Management')]";
    }

    public static String ViewFilterBtn() {
        return "//div[@id='btnActFilter']";
    }

    public static String AddNewBtn() {
        return "//div[@id='btnActAddNew']";
    }

    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }

    public static String IncidentDescription() {
        return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
    }

    public static String IncidentOccured() {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String Mining() {
        return "//a[text()='Mining ']//../i[@class='jstree-icon jstree-ocl']";
        //"a[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String SouthAfrica() {
        return "//a[text()='South Africa']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String VictoryMine() {
        return "//a[text()='Victory Mine']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String Ore() {
        return "//a[text()='Ore Processing']";
    }

    public static String Panel() {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']";
    }

    public static String ProjectLink() {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']//div[@class='c-chk']";
    }

    public static String Location() {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String PinToMap() {
        return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']//div[@class='c-chk']";
    }

    public static String Project() {
        return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']";
    }

    public static String Simon() {
        return "//a[@id='91520b60-bfa6-4392-bb92-a9ebf8abcdd6_anchor']";
    }

    public static String SelectMap() {
        return "//span[text()='Map']";
    }

    public static String RiskDisciple() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']";
    }

    public static String incidenttypeAll() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@original-title='Select all']";
    }

    public static String impacTypeAll() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@original-title='Select all']";
    }

    public static String Compliance() {
        return "//a[@id='b38c4a28-05f9-423d-a53e-3d2940d29f6f_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String SelectAll() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@class='select3-all']";
    }

    public static String IncidentClassification() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']";
    }

    public static String Accident() {
        return "//a[@id='e11fcae9-3784-4677-8a89-5ea95443688b_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Environmental() {
        return "//a[@id='dfa1dbcf-81f5-49b7-a6ba-a070cfbbff19_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Injury() {
        return "//a[@id='75c54c6e-e0f2-49a1-bba3-575131962f3c_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String DatePicker() {
        return "//span[@class='k-widget k-datepicker k-header']";
    }

    public static String Date() {
        return "//a[@title='12 September 2018']";
    }

    public static String ClickUp() {
        return "//b[@class='select3-down drop_click']";
    }

    public static String IncidentTime() {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input[@class='is-timeEntry']";
    }

    public static String Shift() {
        return "//div[@id='control_D47426BB-60CA-49A3-93BA-433B1E8BC4BD']";
    }

    public static String DayShift() {
        return "//a[text()='Day Shift']";
    }

    public static String ActionTaken() {
        return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea[@class='txt']";
    }

    public static String Reported() {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']";
    }

    public static String Save() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@i18n='save']";
    }

    public static String SuperVisor() {
        return "//div[@id='control_F2A12EB0-344C-47E6-BB85-B74B9B53CEFB']//ul[@class='select3-tagbox']";
    }

    public static String SelectAdmin() {
        return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']";
    }

    public static String incidentReportTab() {
        return "//li[@id='tab_82263EDF-CFFD-470B-9B79-ACCD2280758D'][@class='active']";
    }

    public static String supportDocumentTab() {
        return "//li[@id='tab_98124058-DEC1-41A5-82BF-7B07BA53107C']";
    }

    public static String mapOpenTab() {
        return "//div[@id='control_6618F574-B672-4450-93CF-365F64F8CC02']/../../..//div[@class='c-pnl open']";
    }
    
     public static String mapCloseTab() {
        return "//span[text()='Map']";
    }

    public static String userXpath() {
        return "//div[@class='ui dropdown item']/span";
    }
    
    public static String clickNothing(){
        return "//div[contains(text(),'Pin to map')]";
    }

    public static String reportedByAc(String user) {
        return "//li[text()='" + user + "']";
    }

    public static String reportedByChecked() {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']//div[contains(@class, 'checked')]";
    }

    public static String pinMapChecked() {
        return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']//div[contains(@class, 'checked')]";
    }

    public static String linkProjectChecked() {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']//div[contains(@class, 'checked')]";
    }

    public static String externalPartiesChecked() {
        return "//div[@id='control_D086CF05-958D-4916-ADC3-BD45703467A5']//div[contains(@class, 'checked')]";
    }

    public static String addNewExternalParty() {
        return "//div[text()='Add new external party']";
    }

    public static String waitXpath() {
        return "//div[@id='form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']//div[@id='control_4CFF9552-2850-4C36-9EB7-3A97CCB23493']";
    }

    public static String Safety() {
        return "//div[@id='control_D9A59CA2-B160-48B0-87A5-EACCE62378D4']";
    }

    public static String EnvironmentXpath() {
        return "//li[@id='tab_1ACBA560-AF30-4AC3-B7B0-EC5D264E1BD7']";
    }

    public static String QualitytXpath() {
        return "//li[@id='tab_83609D85-9F2F-4BAF-8CB2-887CA05FC034']";
    }

    public static String railwaySafetyXpath() {
        return "//li[@id='tab_C765F87F-EEE5-40F5-AF0F-C52C3C964384']";
    }

    public static String socialSusXpath() {
        return "//li[@id='tab_718C3400-E9C8-4C9A-98D2-87C0E4D43DB6']";
    }

    public static String occHygieneXpath() {
        return "//li[@id='tab_22127382-55B8-49C4-BBF2-89F59794D122']";
    }

    public static String complianceXpath() {
        return "//li[@id='tab_A41DFD6D-7804-41EA-AB3D-9ABF5EDE0BE7']";
    }

    public static String personInvolvedXpath() {
        return "//span[text()='Persons Involved']";
    }

    public static String witnessStatementsXpath() {
        return "//span[text()='Witness Statements']";
    }

    public static String euipmentInvolvedTabXpath() {
        return "//span[text()='Equipment Involved']";
    }
    
    public static String incidentReport(){
        return "//div[text()='1.Incident Report']";
    }

}
