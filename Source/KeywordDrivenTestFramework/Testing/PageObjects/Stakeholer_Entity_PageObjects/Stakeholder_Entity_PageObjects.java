/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author 
 */
public class Stakeholder_Entity_PageObjects extends BaseClass {

     public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }       
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
   
    public static String navigate_Stakeholders(){
        return "//label[text()='Stakeholder Management']";
    }
    
    public static String navigate_StakeholderEntity(){
        return "//label[text()='Stakeholder Entity']";
    }
    
    public static String SE_add(){
        return "//div[text()='Add']";
    }
    
    public static String SE_processflow(){
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String SE_EntityType_dropdown(){
        return "//div[@id='control_121C3080-4A13-4AC2-8640-F2380C230CDC']//li";
    }
    
    public static String SE_EntityType_dropdownValue(String value)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='"+value+"']";
    }  
    
    public static String SE_name(){
        return "//div[@id='control_6B36E56B-4BD2-4A16-AD58-94FE1883EFE2']/div/div//input";
    }
    
    public static String SE_industry_dropdown(){
        return "//div[@id='control_425BD26A-5DDB-485A-BA7F-E8D2E51C4BEA']";
    }
    
    public static String SE_industry_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String SE_desc(){
        return "//div[@id='control_4AEFCBE1-7C06-4528-BB6B-CFD298C47AA1']/div/div//textarea";
    }
    
    public static String SE_owner_dropdown(){
        return "//div[@id='control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1']";
    }
    
    public static String SE_owner_select(String text){
        return "//a[contains(text(),'"+text+"')]";
    }
    
    public static String SE_categories(String text){
        return "//div[@id='control_BDB3E74D-818E-4A51-8443-3F30BA7A472A']//a[contains(text(),'"+text+"')]/i[1]";
    }
    
    public static String SE_business_unit_selectall(){
        return "//div[@id='control_931D1181-0EA6-4EBF-BAA9-B497F5793EC0']//b[@original-title='Select all']";
    }
    
    public static String SE_impact_types_selectall(){
        return "//div[@id='control_962A59FE-78C1-4FCE-B40E-CAC2A8124522']//b[@original-title='Select all']";
    }
   
    public static String SE_savetocontinue(){
        return "//div[@id='control_9E597588-6E9E-4EB7-A607-CC0E20503899']//div[text()='Save to continue']";
    }
    
    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    
    
    //Entinty Information
    public static String entityInformationTab() {
        return "//li[@id='tab_337B1D56-1774-4C9D-8515-CC886C55C1FB']";
    }

    public static String primaryContactNo() {
        return "(//div[@id='control_F58CA39A-8E24-4A56-987E-5B84412F0A9C']//input)[1]";
    }

    public static String secondaryContactNo() {
        return "(//div[@id='control_5FB377EB-8769-48EA-9981-11E3CBF7BEE4']//input)[1]";
    }

    public static String emailAddress() {
        return "(//div[@id='control_E95B004E-1283-4AD5-85C0-2AA11196F34D']//input)[1]";
    }
    
    public static String streetOne() {
        return "(//div[@id='control_C30F3943-2351-490A-A865-7D2337A1F7C5']//input)[1]";
    }
     
    public static String streetTwo() {
        return "(//div[@id='control_95D0CC4B-878A-48CE-8BF1-5149BF5D5FD6']//input)[1]";
    }
    
     public static String se_PostalCode()
    {
        return "//div[@id='control_99DCFFAC-34D1-4291-9154-5B3B7DC59D1B']/div/div/input";
    }
     
    public static String se_Location()
    {
        return "//div[@id='control_B64B2E96-473B-4207-8FCE-C7DAB53F8834']//li";
    }
    
     public static String se_Location_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }
         
    public static String se_Status()
    {
        return "//div[@id='control_AEDC76EE-6EA0-48C6-91D0-A2CE27CEF60D']//li";
    }
    
    public static String se_Status_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
     
    public static String mainContact() {
        return "(//div[@id='control_DB611D04-5A6A-485B-B7FD-3E7468577C4D']//input)[1]";
    }
     
    public static String mainContactPhone() {
        return "(//div[@id='control_1588E4E5-AAB4-4254-8D7D-0ED15A0CC591']//input)[1]";
    }
    
    public static String mainContactEmail() {
        return "(//div[@id='control_D7FD3FAD-217D-47F0-B28F-C2B1AAFF57B3']//input)[1]";
    } 
    
     public static String se_MainContactIndividual()
    {
        return "//div[@id='control_1FE052A1-221D-426B-B9E1-46569A2BA8EF']//li";
    }
    
     public static String se_MainContactIndividual_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
    public static String address() {
        return "//div[@id='control_7A4E044D-6ACD-4B15-A059-EBF7A1947AAF']//textarea";
    }

    public static String correspondenceAddress() {
        return "//div[@id='control_FA9DF216-979C-4C30-98B6-3E6E7961F8BC']//textarea";
    }

    public static String website() {
        return "(//div[@id='control_D2683D07-2857-4D9C-8EF6-588B6625B2EE']//input)[1]";
    }

    public static String SE_save() {
        return "(//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']//div[contains(text(),'Save')])[3]";
    }

    public static String leftArrowTab() {
        return "//div[@class='tabpanel_left_scroll icon chevron left tabpanel_left_scroll_disabled']";
    }

//    public static String rightArrowTab() {
//        return "//div[@class='tabpanel_right_scroll icon chevron right']";
//    }

     public static String rightArrowTab() {
        return "(//div[@class='tabpanel_tab_content']/div)[2]";
    }
     
    
    // actions
    public static String action_description() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    
    public static String departmentResponsibleDropDown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
     
    public static String departmentResponsibleDropDownValue(String value) {
        return "(.//a[text()='"+value+"'])[2]";
    }
        
    public static String actionDueDate() 
    {
        return ".//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    
    public static String StakeholderEntity_Save() 
    {
        return ".//div[@id='btnSave_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }
    
    
        
    //Upload Hyperlink Documents
    public static String supportDocumentsTab() {
        return "//li[@id='tab_144D92B6-733C-4F37-8572-6F268883A0EC']//div[contains(text(),'Supporting Documents')]";
    }
    public static String SE_uploadLinkbox() {
        return "//div[@id='control_BFA08DA7-DF6E-4B10-A435-4EBD13654902']//b[@class='linkbox-link']";
    }
    public static String LinkURL(){
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle(){
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }
    public static String urlAddButton(){
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }
    
    //Engagements
    public static String engagementsTab() {
        return "//li[@id='tab_3942DBC8-B12A-4A79-97F6-4ED66D494909']";
    }
    public static String addEngagementsButton() {
        return "//div[@id='control_A50A638E-99DE-44B8-A7B5-2BF8A948862A']";
    }
    public static String processFlow() {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }
    
     public static String si_engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
        public static String businessUnitTab() {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//li";
    }
    public static String businessUnit_GlobalCompany() {
        return "//a[contains(text(),'Global Company')]";
    }
//    public static String businessUnit_SelectAll() {
//        return "(//span[@class='select3-arrow']//b[@class='select3-all'])[3]";
//    }
    
    public static String businessUnit_SelectAll()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@original-title='Select all']";
    }
    public static String expandGlobalCompany() {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'Global Company')]//..//i)[1]";
    }
    public static String expandSouthAfrica(){
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'South Africa')]//..//i)[1]";
    }
    public static String businessUnitSelect(String text) {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]/i)[1]";
    }
    public static String specificLocation() {
        return "(//div[@id='control_0501A692-460C-4DB5-9EAC-F15EE8934113']//input)[1]";
    }
    public static String projectLink() {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }
    public static String projectTab() {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']//li";
    }
    public static String anyProject(String text) {
        return "//a[contains(text(),'" + text + "')]";
    }
    public static String ShowMapLink() {
        return "//div[@id='control_6723362D-F9E2-457C-B125-13246382C3F7']/div[1]";
    }
    public static String engagementDate() {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
    public static String communicationWithTab() {
       return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']";
    }
    public static String communicationWith_SelectAll() {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']//b[@class='select3-all']";
    }
    public static String communicationUsers() {
        return "//a[contains(text(),'IsoMetrix Users')]";
    }
    public static String usersTab() {
        return "//div[@id='control_CD33B817-1DF7-4499-A055-2A70F29398FC']";
    }
    public static String userOption(String text){
        return "(//a[contains(text(),'"+ text +"')]/i)[1]";
    }
    public static String stakeholdersTab() {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']";
    }
    public static String anyStakeholders(String text) {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }
    public static String Stakeholders_SelectAll() {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']//b[@class='select3-all']";
    }
    public static String entitiesTab() {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']";
    }
    public static String entities(String data) {
        return "(//a[contains(text(),'" + data + "')]/i)[1]";
    }
    public static String expandInPerson() {
        return "(//a[contains(text(),'In-person engagements')]//..//i)[1]";
    }
    public static String expandOtherForms() {
        return "(//a[contains(text(),'Other forms of engagement')]//..//i)[1]";
    }
    public static String anyEngagementMethod(String data) {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }
    public static String engagementMethodTab() {
       return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']";
    }
    public static String engagementPurposeTab() {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//li";
    }
    public static String anyEngagementPurpose(String data) {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }
    public static String engagementTitle() {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }
    public static String engagementDescription() {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }
    public static String engagementPlan() {
        return "(//div[@id='control_F082EFDC-2D10-478A-AB94-BD4A3FB4FE4B']//textarea)[1]";
    }

    public static String managementApprovalActionCheckBox() {
        return "(//div[@id='control_523CF0A9-803B-44F2-8572-BED13C022A03']/div)[1]";
    }

    public static String stakeholderApprovalAction() {
        return "(//div[@id='control_D0FFF631-1DA5-4BD3-A1C8-75A7527E6A34']/div)[1]";
    }
    public static String responsiblePersonTab() {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']//li";
    }
    
    public static String actionsresponsiblePersonTab() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    
            
    public static String numberOfAttendees() {
        return "(//div[@id='control_3D6CBB44-8CCF-4C7F-9150-D5DDD630995E']//input)[1]";
    }
    public static String anyResponsiblePerson(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

     public static String actionsanyResponsiblePerson(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[2]";
    }
     
     public static String impacttypes_selectall()
    {
        return "//div[@id='control_18DD1597-B800-4D1B-B063-A2E539BB3B8A']//b[@original-title='Select all']";
    }
    
    public static String contactEnquiryOrTopic()
    {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//li";
    }
    
    public static String contactEnquiryOrTopicValue(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
     public static String location()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//li";
    }
    
    public static String locationValue(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "']";
    }
    
    public static String Engagement_save() {
        return "(//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[contains(text(),'Save')])[3]";
    }

    public static String entitiesCloseDropdown() {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']//b[@class='select3-down drop_click']";
    }
    public static String projectTitle(){
        return "IsoMetrix";
    }

    public static String searchTab() {
        return "(//div[contains(text(),'Search')])[5]";
    }
    public static String searchButton() {
        return "(//div[contains(text(),'Search')])[6]";
    }

    public static String engagementsGridView() {
        return "(//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//div//table)[3]";
    }
    //FR2
    public static String members_tab(){
        return "//div[text()='Members']";
    }
    
    public static String member_add(){
        return "(//div[text()='Add'])[2]";
    }
    
    public static String member_name_dopdown(){
        return "//div[@id='control_623A6228-2A56-4954-91D6-D2E07B56E612']//li";
    }
    
    public static String member_name_select(String text){
        return "(//a[text()='"+text+"'])[2]";
    }
    
    public static String member_position(){
        return "//div[@id='control_8C0CA237-7663-407A-A383-71A76D340D97']//li";
    }
    
     public static String member_Position_select(String text){
        return "(//a[text()='"+text+"'])[1]";
    }
     
//    public static String member_save(){
//        return "(//div[text()='Save'])[3]";
//    }
    
    public static String member_save(){
        return "//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String si_fname(){
        return "//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']/div/div/input";
    }
    
    public static String si_lname(){
        return "//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']/div/div/input";
    }
    
    public static String si_knownas(){
        return "//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']/div/div/input";
    }
    
    public static String si_title_dropdown(){
        return "//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']//li";
    }
    
    public static String si_title(String text){
        return "//a[text()='"+text+"']";
    }
    
     public static String si_relationshipOwner_dropdown()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//li";
    }

     public static String si_relationshipOwner_dropdownValue(String text)
    {
        return "//a[text()='" + text + "']";
    }
     
    public static String actions_tab(){
        return "//div[text()='Actions']";
    }
    
    public static String actions_add(){
        return "//div[@id='control_6AECC604-74CD-4CA2-8014-252301E5A6E8']//div[text()='Add']";
    }
    
    public static String actions_pf(){
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }
    
    public static String actions_pcn(){
        return "//div[@id='control_B37A5BE9-D18E-4D38-97C8-A79B0143F45A']/div/div/input";
    }
    
    public static String actions_scn(){
        return "//div[@id='control_5FB377EB-8769-48EA-9981-11E3CBF7BEE4']/div/div/input";
    }
    
    public static String actions_address(){
        return "//div[@id='control_7A4E044D-6ACD-4B15-A059-EBF7A1947AAF']/div/div/textarea";
    }
    
    public static String actions_caddress(){
        return "//div[@id='control_FA9DF216-979C-4C30-98B6-3E6E7961F8BC']/div/div/textarea";
    }
    
    public static String actions_website(){
        return "//div[@id='control_D2683D07-2857-4D9C-8EF6-588B6625B2EE']/div/div/input";
    }
    
//    public static String actions_Save(){
//        return "(//div[text()='Save'])[2]";
//    }
    
    public static String actions_Save(){
        return "//div[@id='btnSave_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }
    
    public static String si_processflow(){
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }
    
    public static String si_designation(){
        return "//div[@id='control_DE0171C4-DDEA-47BD-A5D5-F4DF639EC9E2']/div/div/input";
    }
    
    public static String si_dob(){
        return "//div[@id='control_501DB50D-5488-43AF-91D3-B74D77CC09A3']//input";
    }
    
    public static String si_age(){
        return "//div[@id='control_1BC13F9E-47D7-436E-A5BB-97E88B8D86F5']/div/input";
    }
    
    public static String si_idnum(){
        return "//div[@id='control_5A680F16-7C29-49C2-9698-68C906C1F35F']/div/div/input";
    }
    
    public static String si_gender_dropdown(){
        return "//div[@id='control_BC0B3729-5DA7-493E-BA90-653F5C3E6151']";
    }
    
    public static String si_gender_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String si_nationality_dropdown(){
        return "//div[@id='control_447AC363-4DFF-4821-AB92-6695191EC822']";
    }
    
    public static String si_nationality_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String profile_tab(){
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[text()='Profile']";
    }
    
    public static String si_stakeholdercat_selectall(){
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }
    
    public static String si_stakeholdercat(String text){
        return "//a[text()='"+text+"']/i[1]";
    }
    
    public static String si_appbu_selectall(){
        return "//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']//b[@original-title='Select all']";
    }
    
    public static String si_appbu_expand(String text){
        return "//a[text()='"+text+"']/../i";
    }
    
    public static String si_appbu(String text){
        return "//a[text()='"+text+"']/i[1]";
    }
    
    public static String si_impacttypes_selectall(){
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//b[@original-title='Select all']";
    }
    
    public static String si_impacttypes(String text){
        return "//a[text()='"+text+"']/i[1]";
    }
    public static String si_locationmarker(){
        return "//div[@id='control_6A7B2112-2526-41F1-893D-2702E81144D7']//div[@class='icon location mark mapbox-icons']";
    }
    
    public static String location_tab(){
        return "//div[text()='Location']";
    }
    
    public static String si_location(){
        return "jozi3.PNG";
    }
    public static String si_save(){
        return "(//div[text()='Save'])[2]";
    }
    public static String contractorSupplierManager_Tab() {
        return "//li[@id='tab_5649976E-D3E2-4B5D-8E83-DECD2901F3A0']//div[contains(text(),'Contractor or Supplier Manager')]";
    }
    public static String questionnaires_Panel() {
        return "//span[contains(text(),'Questionnaire')]";
    }
    public static String questionnaires_add() {
        return "//div[@id='control_7517949D-9F94-4E11-8C20-9CE0EF740490']//div[contains(text(),'Add')]";
    }

    public static String questionnaireProcessFlow() {
        return "//div[@id='btnProcessFlow_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']";
    }

    public static String documents_Panel() {
        return "//span[contains(text(),'Documents')]";
    }

    public static String documents_add() {
        return "//div[@id='control_1D721996-A61A-40FD-91E8-7FAFDF3B5039']//div[contains(text(),'Add')]";
    }

    public static String documents_Input() {
        return "(//div[@id='control_4EC7578A-B88B-4442-AA32-754B27F4FDF3']//input)[1]";
    }

    public static String documentsUploaded_Dropdown() {
        return "//div[@id='control_F3FF8316-9847-4F33-ACCE-CEDA5D02BE01']//li";
    }

    public static String documentsUploaded_Dropdown_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String outcome_Dropdown_Option(String data) {
        return "(//a[text()='" + data + "'])[1]";
    }

    public static String outcome_Dropdown() {
        return "//div[@id='control_6BF2BC67-11B6-45D8-9A90-E7A8FB55DDAE']//li";
    }

    public static String dateVerifiedTab() {
        return "(//div[@id='control_1FD49B28-3FF0-4931-9C73-2E3D2D7AA68E']//span//input)[1]";
    }

    public static String commentsTab() {
        return "(//div[@id='control_1FF7D5F1-8209-4446-ABE0-D0D89B11FF22']//textarea)[1]";
    }

    public static String documents_Save() {
        return "(//div[@id='btnSave_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']//div[contains(text(),'Save')])[3]";
    }

    public static String supporting_tab(){
        return "//div[text()='Supporting Documents']";
    }
    
    public static String linkbox(){
        return "//div[@id='control_1F0A25D4-9905-4913-BC61-B1EED48CCCC6']//b[@original-title='Link to a document']";
    }
  
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    
    public static String si_savetocontinue(){
        return "//div[text()='Save supporting documents']";
    }
    
    public static String se_createnewindividual(){
        return "//div[text()='Create a new individual']";
    }
    
    public static String si_rightarrow(){
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[@class='tabpanel_right_scroll icon chevron right']";
    }
    
    public static String sik_rightarrow(){
        return "rightarrow.PNG";
    }
    
    public static String ri_ename(){
        return "//div[@class='optionsPanel firstPanel primary']//td[text()='Entity name']/../td[5]/input";
    }
//    public static String ri_esearch(){
//        return "//div[text()='Search']";
//    }
    
     public static String ri_esearch(){
        return "//div[@id='btnActApplyFilter']";
    }
    
    public static String ri_eselect(String text){
        return "(//div[text()='"+text+"'])[1]";
    }
    public static String ri_related(){
        return "(//div[text()='Related Incidents'])[1]";
    }
    public static String ri_record(){
        return "//div[@id='control_AE8EC219-4756-40BA-8773-68B10FE425BA']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }
    public static String ri_wait(){
        return "(//div[text()='Incident Management '])[1]";
    }

    public static String supportDocumentsTab_CQ() {
        return "//li[@id='tab_88BC6C8F-E230-4853-BE1A-9E90200423E0']//div[contains(text(),'Supporting Documents')]";
    }
    public static String uploadLinkbox() {
        return "(//div[contains(text(),'Supporting Documents')]//..//div[@class='linkbox-options']//b[@class='linkbox-link'])[2]";
    }
    public static String saveToContinueButton(){
        return "//div[@id='control_D4D50C09-26C5-410D-9549-2CC4BE86A134']//div[contains(text(),'Save to continue')]";
    }
    public static String saveSupportingDocuments(){
        return "//div[@id='btnSave_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']";
    }

    public static String companyType_Dropdown() {
        return "//div[@id='control_D8B07754-94EC-4FB2-859C-B738B6EAFD91']//li";
    }

    public static String companyType_Option(String data) {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String relevantEntity_RegNo() {
        return "(//div[@id='control_D6E2582F-135F-475C-8180-054CA159CFCC']//input)[1]";
    }

    //Tabs
    public static String companyInformationTab() {
        return "//li[@id='tab_2AED1AEA-A885-4D65-8C46-A4E0FDD8A034']";
    }
    public static String businessPartnerTab() {
       return "//li[@id='tab_ECC116DB-F2F9-49D9-82C8-122332F6CCC8']";
    }
    public static String companyCertificatesTab() {
       return "//li[@id='tab_73A0738E-B7E2-4F10-9CE7-EF3C31E48A89']";
    }
    public static String safetyHealthIssuesTab() {
       return "//li[@id='tab_44F21890-1F73-40D6-BCEF-0696F8566A3A']";
    }
    public static String healthSafeyEnvironmentTab() {
       return "//li[@id='tab_B01371BE-B0A1-4F0F-A195-7DD5D7BC264E']";
    }
    public static String additionalCompanyInformationTab() {
       return "//li[@id='tab_25BDF161-DD62-438F-B244-F3BF5FD82B72']";
    }
    public static String businessPartnerRiskRankingTab() {
       return "//li[@id='tab_CA730638-F9B1-4552-AE71-1892F034122D']";
    }
    
    //Data
    public static String mainArea() {
        return "(//div[@id='control_349C41BC-5C52-4311-B0AD-5203F0596F74']//input)[1]";
    }
    public static String operatingUnit() {
        return "(//div[@id='control_7236A83F-F382-487E-A660-2213CFC22BB0']//input)[1]";
    }
    public static String qualityManagement_Dropdown() {
        return "//div[@id='control_1575F13D-D625-43F4-AAF6-3462C227AD3A']//li";
    }

    public static String qualityManagement_Option(String data) {
        return "(//div[@id='control_3AA7ED92-601B-477C-9BF2-A248733FD534']/../../../../../../../..//a[text()='" + data + "'])[1]";
    }

    public static String environmentalManagement_Dropdown() {
        return "//div[@id='control_749B58E4-1696-44A2-A2B5-6544169861E1']//li";
    }

    public static String environmentalManagement_Option(String data) {
         return "(//div[@id='control_0D0FD72F-3263-4222-82C9-5964A0D96B95']/../../../../../../../..//a[text()='" + data + "'])[2]";
    }

    public static String COID_Act_Dropdown() {
        return "//div[@id='control_2BF947B0-C0CE-4F29-BEB7-EEAD413BCABF']//li";
    }

    public static String COID_Act_Option(String data) {
        return "(//div[@id='control_D8A26477-3F38-4CA0-A72D-19E94C7B0F3A']/../../../../../../../..//a[text()='" + data + "'])[3]";
    }

    public static String COID_RegNo() {
       return "(//div[@id='control_D70BA03D-C157-42B0-9C6E-4F1A62106056']//input)[1]";
    }

    public static String rightArrowButton() {
        return "//div[@id='control_5554718D-1C5B-401C-AE1E-6ABF12EDB801']//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String legalRegister_Dropdown() {
        return "//div[@id='control_AC471A15-AE17-44CA-9330-9110BEAA186E']//li";
    }

    public static String legalRegister_Option(String data) {
        return "(//div[@id='control_3C1ADF6C-2A91-4289-B91B-FBA0B3C43FE4']/../../../../../../../..//a[text()='" + data + "'])[4]";
    }

    public static String healthSafetyPlan_Dropdown() {
        return "//div[@id='control_3C62267D-11AA-4E60-AEB8-7F89010D81AE']";
    }

    public static String healthSafetyPlan_Option(String data) {
        return "(//div[@id='control_204F364D-A254-4AA9-BE1C-0089EE76B00F']/../../../../../../../..//a[text()='" + data + "'])[5]";
    }

    public static String rentedOwnedLeased_Dropdown() {
        return "//div[@id='control_CC9B150A-E4AF-47F4-82A2-A191DBC23DDC']//li";
    }

    public static String rentedOwnedLeased_Option(String data) {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String furnishAnnualNettProfit() {
        return "(//div[@id='control_86D35ED9-FA92-4695-8B43-9CFBE975017E']//input)[1]";
    }

    public static String questionnaire_Save() {
        return "//div[@id='btnSave_form_7B9DCBC8-678E-4ED9-8CF5-C5AA78BED2D8']//div[text()='Save']";
    }

    public static String scopeOfWork() {
        return "(//div[@id='control_32D49E9F-7654-44D8-89AB-9A24D0E343A4']//textarea)[1]";
    }
    
    public static String report(){
        return "//div[@id='btnReports']";
    }
    
    public static String view_reports(){
        return "//span[@title='View report ']";
    }

    public static String full_Reports(){
        return "//span[@title='Full report ']";
    }
    
    public static String view_wait(){
        return "//div[text()='Entity report']";
    }
    
    public static String full_wait(){
        return "//div[text()='Entity status']";
    }
    
//    public static String popup_conf(){
//        return "//div[@id='btnConfirmYes']";
//    }
//    
    public static String popup_conf(){
        return "//div[@title='Continue']";
    }
    
    
    public static String stakeholder_Array(String data){
        return "(//a[text()='" + data + "']/i[1])";
    }

    public static String anyStakeholders2(String text,int counter) {
        return "(//a[contains(text(),'" + text + "')]/i[1])[ " + counter + " ]";
    }
    public static String se_locationmarker(){
        return "//div[@id='control_D451A141-6811-4844-9A2C-501A338A05C7']//div[@class='icon location mark mapbox-icons']";
    }
    public static String inspection_RecordSaved_popup() {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String recordSaved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
    }
    public static String failed(){
       return "(//div[@id='txtAlert'])[2]";
   }
}
