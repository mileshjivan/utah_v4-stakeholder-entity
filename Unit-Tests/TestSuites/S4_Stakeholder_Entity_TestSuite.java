/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class S4_Stakeholder_Entity_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S4_Stakeholder_Entity_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    //FR1 - Capture Stakeholder Entity - Main Scenario
    @Test
    public void S4_StakeholderEntity_Beta() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\S4_Stakeholder Entity_Beta.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    //FR1 - Capture Stakeholder Entity - Main Scenario
    @Test
    public void FR1_Capture_Stakeholder_Entity_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture Stakeholder Entity - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Stakeholder_Entity_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture Stakeholder Entity - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC STE 01-02: Capture stakeholder entity additional details - Main Scenario
    @Test
    public void UC_STE_01_02_Capture_Stakeholder_Entity_Additional_Details_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\UC_STE_01_02_Capture _Stakeholder_Entity_Information - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void UC_STE_01_02_Capture_Stakeholder_Entity_Additional_Details_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\UC_STE_01_02_Capture _Stakeholder_Entity_Information - OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2 - Capture Related Stakeholder - Main Scenario
    @Test
    public void FR2_Capture_Related_Stakeholder_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2-Capture_Related_Stakeholder - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR2_Capture_Related_Stakeholder_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2-Capture_Related_Stakeholder - Alternate_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR3 – Add/View Engagements - Main Scenario
    @Test
    public void FR3_Add_ViewEngagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR3-AddView_Engagements_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR3_Add_ViewEngagements_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR3-AddView_Engagements_OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR4 - Capture Stakeholder Entity Actions - Main Scenario
    @Test
    public void FR4_Capture_Stakeholder_Entity_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR4-Capture_Stakeholder_Entity_Actions - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5 – View Related Incidents - Main Scenario
    @Test
    public void FR5_View_Related_Incidents_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR5-View Related Incidents - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR6 – Capture Contractor Questionnaire - Main Scenario
    @Test
    public void FR6_CaptureContractorQuestionnaire_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR6-Capture_Contractor_Questionnaire - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR6_CaptureContractorQuestionnaire_OptionalScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR6-Capture_Contractor_Questionnaire - OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR7 – Capture Contractor Documents - Main Scenario
    @Test
    public void FR7_Capture_Contractor_Documents_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR7_Capture_Contractor_Documents_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR8 – View Dashboards - Main Scenario
    @Test
    public void FR8_View_Dashboards_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR8 – View Dashboards - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR9 - View Reports - Main Scenario
    @Test
    public void FR9_View_Reports_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR9-View Reports - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
    
}
