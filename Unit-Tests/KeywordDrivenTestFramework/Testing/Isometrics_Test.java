package KeywordDrivenTestFramework.Testing;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class Isometrics_Test
{

    static TestMarshall instance;

    public Isometrics_Test()
    {

    }

    @Test
    public void testRunKeywordDrivenTests() throws FileNotFoundException
    {
        Narrator.logDebug("runKeywordDrivenTests");
        instance = new TestMarshall("TestPacks\\Findings v5\\FR4-View Findings v5 - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.currentEnvironment = Enums.Environment.v5QA;
        instance.runKeywordDrivenTests();
    }

}
